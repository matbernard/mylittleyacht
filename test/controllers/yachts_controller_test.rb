require 'test_helper'

class YachtsControllerTest < ActionController::TestCase
  setup do
    @yacht = yachts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:yachts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create yacht" do
    assert_difference('Yacht.count') do
      post :create, yacht: { cabins_number: @yacht.cabins_number, crew_number: @yacht.crew_number, description: @yacht.description, guests_number: @yacht.guests_number, harbor: @yacht.harbor, length: @yacht.length, name: @yacht.name, skipper_id: @yacht.skipper_id, title: @yacht.title, type: @yacht.type, year: @yacht.year }
    end

    assert_redirected_to yacht_path(assigns(:yacht))
  end

  test "should show yacht" do
    get :show, id: @yacht
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @yacht
    assert_response :success
  end

  test "should update yacht" do
    patch :update, id: @yacht, yacht: { cabins_number: @yacht.cabins_number, crew_number: @yacht.crew_number, description: @yacht.description, guests_number: @yacht.guests_number, harbor: @yacht.harbor, length: @yacht.length, name: @yacht.name, skipper_id: @yacht.skipper_id, title: @yacht.title, type: @yacht.type, year: @yacht.year }
    assert_redirected_to yacht_path(assigns(:yacht))
  end

  test "should destroy yacht" do
    assert_difference('Yacht.count', -1) do
      delete :destroy, id: @yacht
    end

    assert_redirected_to yachts_path
  end
end
