class CreateYachts < ActiveRecord::Migration
  def change
    create_table :yachts do |t|
      t.belongs_to :skipper
      t.string :name
      t.string :title
      t.text :description
      t.string :type
      t.float :length
      t.string :harbor
      t.integer :year
      t.integer :guests_number
      t.integer :crew_number
      t.integer :cabins_number

      t.timestamps
    end
  end
end
