json.array!(@yachts) do |yacht|
  json.extract! yacht, :id, :skipper_id, :name, :title, :description, :type, :length, :harbor, :year, :guests_number, :crew_number, :cabins_number
  json.url yacht_url(yacht, format: :json)
end
