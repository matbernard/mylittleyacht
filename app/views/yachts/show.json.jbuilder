json.extract! @yacht, :id, :skipper_id, :name, :title, :description, :type, :length, :harbor, :year, :guests_number, :crew_number, :cabins_number, :created_at, :updated_at
