class Yacht < ActiveRecord::Base
	belongs_to :skipper, class_name: 'User'

	has_many :pictures, dependent: :destroy
	accepts_nested_attributes_for :pictures
	validates :name, :title, :cabins_number, presence:true
end
